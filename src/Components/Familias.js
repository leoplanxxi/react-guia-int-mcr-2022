import React,{Component} from 'react';
import '../App.css';

class Familias extends Component {
	constructor() {
		super();
	}

	render() {
		return(
			<div>
				<label for={this.props.id}><input type="checkbox"  value={this.props.value} id={this.props.id} /> {this.props.label}</label>
				
			</div>
		);
	}
}

export default Familias;
